from setuptools import setup, find_packages
setup(
    name="createvoxel",
    version="0.1",
    packages=find_packages(),
    scripts=['cv'],

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires=['rasterio>=1.0', 'numpy', 'shapely', 'laspy>=1.5.0'],

    package_data={
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.txt', '*.rst', 'cv'],
    },

    # metadata to display on PyPI
    author="Jeremy Castagno",
    author_email="jdcasta@umich.edu",
    description="Creates 3D voxel environments from GIS data",
    license="MIT",
    keywords="voxel elevation lidar las",
    url="https://bitbucket.org/umich_a2sys/create-voxel/src/master/",   # project home page, if any
    project_urls={
        "Bug Tracker": "https://bitbucket.org/umich_a2sys/create-voxel/src/master/",
    }

)