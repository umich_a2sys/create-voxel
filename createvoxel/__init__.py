"""Generates voxel environment

This module will generate a georeferenced voxel environment
Data sources:
    - Terrain Elevation (GeoTIFF)
    - Spatialite Database of buildings

"""

import math

import rasterio
from rasterio import features
from shapely import wkb
import numpy as np

from createvoxel.dbconn import DBConn


def merge_3D_to_2D(three_d):
    depth = three_d.shape[2]
    final = np.copy(three_d[:, :, depth - 1])
    for i in reversed(range(0, depth - 1)):
        mask = three_d[:, :, i] == i + 1
        final[mask] = i + 1
    return final


def real_to_index(val, val_min, val_max, res=2.0):
    index = math.floor((val - val_min) / res)
    return min(max(index, 0), (val_max - val_min) / 2.0)


def index_to_real(index, val_min, res=2.0):
    real = val_min + index * res
    return real


def combine_potential_voxels(pf_cells_list, potential_field_depth=1):
    max_cells = potential_field_depth - 1
    all_voxels = pf_cells_list[max_cells].copy()
    # last one is the farthest away, lowest risk 1
    # seconds last one has risk of 2
    # first one has a risk of 3
    for i in reversed(range(max_cells)):
        pf_cells = pf_cells_list[i]
        mask = (pf_cells == (potential_field_depth - i))
        all_voxels[mask] = potential_field_depth - i
    return all_voxels


def overlap(a, b):
    return a[1] >= b[0] and a[0] <= b[1]

# def filter_indices(index_2d, nrows, ncols):


class Voxelizer():
    """Class that creates voxel environment

    """

    def __init__(self, file_path_elevation, buffer_top=14, max_height_diff=120):
        """Class that creates voxel environment from terrain and building information

        Arguments:
            file_path_elevation {str} -- Path to elevation GeoTif

        Keyword Arguments:
            buffer_top {int} -- How many meter to buffer the environment from the heighest elevation height (default: {13})
        """

        src = rasterio.open(file_path_elevation)
        self.affine = src.meta['transform']           # affine transform
        self.srid = src.meta['crs']['init']           # e.g. - 'epsg:5555'
        self.srid_num = int(self.srid.split(':')[1])  # e.g. - 5555

        ncols = src.meta['width']
        nrows = src.meta['height']

        # get the resolution of the map (meters)
        xres = round(self.affine.a)
        yres = abs(round(self.affine.e))
        zres = xres
        # get minimum values in the desired projected coordinate system
        xmin = self.affine.c
        ymin = self.affine.f if self.affine.e > 0 else self.affine.f + nrows * self.affine.e
        # Read the height values
        self.height_values = src.read(1)
        zmin = self.height_values.min()
        height_diff = int(self.height_values.max() - zmin)
        height_diff += int(buffer_top)
        height_diff = min(height_diff, max_height_diff)
        # set height values to this new max
        too_high = self.height_values >= zmin + max_height_diff
        self.height_values[too_high] = zmin + max_height_diff
        nslices = math.ceil(height_diff / zres)
        # Create a dictionary of meta information for our voxel coordinate system
        self.meta = dict(srid=self.srid, nrows=nrows, ncols=ncols, nslices=nslices,
                         xres=xres, yres=yres, zres=zres, xmin=xmin, ymin=ymin, zmin=zmin)

        # Holds 3D voxels for terrain, buildings, and potential field data
        self.terrain_voxels = np.zeros((nrows, ncols, nslices), dtype=np.uint8)
        self.building_voxels = None         # Created dynamically later if user needs
        self.potential_field_voxels = None  # Created dynamically later if user needs

        self.buildings = []
        self.expanded_buildings = []

        src.close()

    def voxelize_terrain(self, ground_buffer=10, terrain_value=255):
        """Creates a voxel grid based on terrain elevation

        Keyword Arguments:
            ground_buffer {int} -- How many voxels cells to fill underneath a ground voxel (default: {10})
            terrain_value {int} -- Cell value representing a ground point  (default: {255})
        """

        min_val = self.meta['zmin']
        zres = self.meta['zres']
        z_dim = self.meta['nslices']
        for i in range(z_dim):
            height = min_val + i * zres
            low_i = max(i - ground_buffer, 0)
            mask = (self.height_values >= height) & (
                self.height_values <= height + zres)
            # print(mask)
            self.terrain_voxels[mask, low_i:i + 1] = terrain_value
        return self.terrain_voxels

    def get_pf_indexes(self, index_2d, manh_dist):
        list_indices = []
        if manh_dist == 1:
            list_indices.append((index_2d[0] - 1, index_2d[1]))
            list_indices.append((index_2d[0] + 1, index_2d[1]))
            list_indices.append((index_2d[0], index_2d[1] + 1))
            list_indices.append((index_2d[0], index_2d[1] - 1))
        elif manh_dist == 2:
            list_indices.append((index_2d[0] - 2, index_2d[1]))
            list_indices.append((index_2d[0] + 2, index_2d[1]))
            list_indices.append((index_2d[0], index_2d[1] + 2))
            list_indices.append((index_2d[0], index_2d[1] - 2))
            # Corners
            list_indices.append((index_2d[0] - 1, index_2d[1] - 1))
            list_indices.append((index_2d[0] - 1, index_2d[1] + 1))
            list_indices.append((index_2d[0] + 1, index_2d[1] + 1))
            list_indices.append((index_2d[0] + 1, index_2d[1] - 1))
        elif manh_dist == 3:
            # Corners
            list_indices.append((index_2d[0] - 1, index_2d[1] - 2))
            list_indices.append((index_2d[0] - 1, index_2d[1] + 2))
            list_indices.append((index_2d[0] + 1, index_2d[1] + 2))
            list_indices.append((index_2d[0] + 1, index_2d[1] - 2))

            list_indices.append((index_2d[0] - 2, index_2d[1] - 1))
            list_indices.append((index_2d[0] - 2, index_2d[1] + 1))
            list_indices.append((index_2d[0] + 2, index_2d[1] + 1))
            list_indices.append((index_2d[0] + 2, index_2d[1] - 1))

            list_indices.append((index_2d[0] - 3, index_2d[1]))
            list_indices.append((index_2d[0] + 3, index_2d[1]))
            list_indices.append((index_2d[0], index_2d[1] + 3))
            list_indices.append((index_2d[0], index_2d[1] - 3))

        # list_indices = [ for list_indices]
        return list_indices

    def create_potential_field_all(self, potential_field_depth=1, scale=True):
        """Creates a potential field around every occupied cell provided by self.height_values
        This is useful if heigh_values represents a DSM and you want a potential field around buldings as well

        Keyword Arguments:
            potential_field_depth {int} -- number of cells outward of the potential field (default: {1})
            scale {bool} -- Scale risk to 255 (default: {True})

        Returns:
            numpy -- 3D numpy array
        """

        # get meta data information
        min_val = self.meta['zmin']
        zres = self.meta['zres']
        z_dim = self.meta['nslices']
        x_dim = self.meta['nrows']
        y_dim = self.meta['ncols']

        # contains multiple temporary 3D voxel maps, each one representing a level of the potential field
        # e.g. the first contains 3D voxel map contains all points 1 manhattan distance away from an obstacle
        # e.g. the second contains a 3D voxel map of all points 2 manhattan distance away
        pf_cells_list = []
        for i in range(potential_field_depth):
            pf_cells_list.append(np.zeros_like(self.terrain_voxels))

        # Efficient multi index iteration of a multidimensional numpy array (2D Elevation Map)
        # C code should be about 3-5 times faster
        it = np.nditer(self.height_values, flags=['multi_index'])
        while not it.finished:
            # if it[0] >= 1:
            # row,col index of this cell
            index_2d = it.multi_index
            # it[0] is height value at this row/col cell
            # getting the z_dim
            k = math.ceil((it[0] - min_val) / zres)
            for i in range(potential_field_depth):
                # list of indices (i,j) that are within i+1 manhattan distances away from this cell
                new_indices_2d = self.get_pf_indexes(index_2d, i + 1)
                pf_cells = pf_cells_list[i]
                for to_write in new_indices_2d:
                    if to_write[0] >= 0 and to_write[0] < x_dim and to_write[1] >= 0 and to_write[1] < y_dim:
                        # current height level AND DOWN (side of buildings)
                        # e.g. 3 - i, i.e. Manh dist of 1 is level 3 risk
                        pf_cells[to_write[0], to_write[1],
                                 :k+1] = potential_field_depth - i

                        # cells above (top of buildings)
                        if k + i + 1 < z_dim and i != 2:
                            pf_cells = pf_cells_list[i + 1]
                            pf_cells[to_write[0], to_write[1], k +
                                     i + 1] = potential_field_depth - i - 1
            it.iternext()

        # important magic happens here. We merge all these temporary 3D voxel maps into one
        # if there is a collision, the cell with higher value risk wins
        self.potential_field_voxels = combine_potential_voxels(
            pf_cells_list, potential_field_depth=potential_field_depth)
        # Values are like 1, 2, 3, scale to 255
        if scale:
            scale_val = math.floor(255 / (potential_field_depth + 1))
            self.potential_field_voxels = self.potential_field_voxels * scale_val

        return self.potential_field_voxels

    def prepare_buildings(self, **kwargs):
        """Queries buildings from database
        Saves buildings in class attribute
        """
        # Use srid of elevation file if none provided
        if kwargs.get('srid') is None:
            kwargs['srid'] = self.srid_num
        self.dbconn = DBConn(**kwargs)
        self.buildings = self.dbconn.get_buildings(buffer=0)
        return self.buildings

    def rasterize_buildings(self, map_slice, buildings, value=255):
        """Rasterizes building geometry into a numpy raster

        Arguments:
            map_slice {numpy} -- 2D numpy array
            buildings {list} -- List of dictionary of buildings.

        Keyword Arguments:
            value {int} -- [description] (default: {255})
        """

        build_features = []
        for building in buildings:
            shape = wkb.loads(building['wkb'])
            build_features.append((shape, value))
        features.rasterize(build_features, out_shape=map_slice.shape,
                           out=map_slice, transform=self.affine)

    def voxelize_buildings(self, buildings, value=255):
        """Voxelizes buildings into a 3D numpy array

        Arguments:
            buildings {list} -- List of dictionary of buildings

        Keyword Arguments:
            value {int} -- Value a building cell takes (default: {255})

        Returns:
            numpy -- 3D numpy array
        """

        z_dim = self.meta['nslices']
        z_min = self.meta['zmin']
        z_res = self.meta['zres']
        self.building_voxels = np.zeros_like(self.terrain_voxels)
        for i in range(z_dim):
            low_real = index_to_real(i, z_min, z_res)
            high_real = index_to_real(i + 1, z_min, z_res)
            lyr_buildings = [building for building in buildings if overlap(
                [building[self.dbconn.ground_height_col], building[self.dbconn.total_height_col]], [low_real, high_real])]
            if lyr_buildings:
                self.rasterize_buildings(
                    self.building_voxels[:, :, i], lyr_buildings, value=value)
        return self.building_voxels

    def get_expanded_buildings(self, max_cells=3):
        """Returns a list of lists. Each internal list contains a set of buildings whose geometries have been expanded a configurable amount.
        A total of max_cell (3) internal lists are created. Where each of these lists contains building geometries with progressively
        expanded geometries. The first list is expanded by zres (2 meters) by applying a radial buffer around the geometry.  The last list is
        expanded by max_cell * zres (6 meters).
        Keyword Arguments:
            max_cells {int} -- How many cells to outward of a building to create a potential field (default: {3})

        Returns:
            list of list -- List of list of buildings. Each internal list contains a set of buildings whose geometries have been expanded a configurable amount.
        """

        res = self.meta['zres']
        # e.g. 2, 4, 6
        for dist in range(res, (max_cells + 1) * res, res):
            result = self.dbconn.get_buildings(dist)
            self.expanded_buildings.append(result)
        return self.expanded_buildings

    def filter_expanded_buildings(self, building_list, low_real, high_real, res=2):
        expanded_buildings_filtered = []
        build_depth = len(building_list)
        # TODO: Performance - The same buildings are being filtered on each of iteration of the for loop.
        for idx, buildings in enumerate(building_list):
            side_top_buildings = [building for building in buildings if overlap([building[self.dbconn.ground_height_col], building[self.dbconn.total_height_col]], [
                                  low_real, high_real]) or (low_real - building[self.dbconn.total_height_col] < build_depth * res and low_real - building[self.dbconn.total_height_col] >= 0)]
            expanded_buildings_filtered.append(side_top_buildings)

        return expanded_buildings_filtered

    def create_potential_field(self, scale=True):
        """Creates a potential field around buildings

        Keyword Arguments:
            scale {bool} -- Whether to scale the potential field values (default: {True})

        Returns:
            numpy -- 3D numpy array representing a potential field around buildings in the voxel map
        """

        z_dim = self.meta['nslices']
        z_min = self.meta['zmin']
        z_res = self.meta['zres']
        self.potential_field_voxels = np.zeros_like(self.terrain_voxels)
        field_cell_depth = len(self.expanded_buildings)
        # Will hold the potential field slices, radiating outward from a building
        potential_field_temp = np.zeros(
            shape=(self.terrain_voxels.shape[0:2] + (field_cell_depth,)))
        for i in range(z_dim):
            potential_field_temp[...] = 0  # reset after each iteration
            # The real world height range this slice in the z dimension is referring to
            low_real = index_to_real(i, z_min, z_res)
            high_real = index_to_real(i + 1, z_min, z_res)
            expanded_buildings = self.filter_expanded_buildings(
                self.expanded_buildings, low_real, high_real)

            # rasterize each outward layer into the temporary potential field
            for j in range(field_cell_depth):
                if expanded_buildings[j]:
                    self.rasterize_buildings(
                        potential_field_temp[:, :, j], expanded_buildings[j], value=j + 1)
            # Merge the potential field layers into one single 2D image, making sure never to overwrite lower numbers (penalties of cell being close to buildings)
            self.potential_field_voxels[:, :, i] = merge_3D_to_2D(
                potential_field_temp)

        # Scale the
        if scale:
            scale_val = math.floor(255 / (field_cell_depth + 1))
            self.potential_field_voxels = self.potential_field_voxels * scale_val
        return self.potential_field_voxels

    @staticmethod
    def combine_voxels(terrain, building=None, potential_field=None):
        """Combines terrain, building, and optionally a potential field voxel grid

        Arguments:
            terrain {numpy} -- Terrain 3D numpy voxel map
            building {numpy} -- Building 3D numpy voxel Map

        Keyword Arguments:
            potential_field {numpy} -- Potential Field 3D numpy voxel map (default: {None})

        Returns:
            numpy -- Combined 3D numpy voxel map
        """

        if potential_field is not None:
            all_voxels = potential_field.copy()
            mask = terrain == terrain.max()
            all_voxels[mask] = terrain.max()
            if building is not None:
                mask = building == building.max()
                all_voxels[mask] = building.max()
        elif building is not None:
            all_voxels = terrain.copy()
            mask = building == building.max()
            all_voxels[mask] = building.max()
        else:
            all_voxels = terrain
        return all_voxels
