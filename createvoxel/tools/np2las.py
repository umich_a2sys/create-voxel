import argparse

import numpy as np

from laspy.file import File, header


def voxel_to_numpy(voxels, min_value=1):
    reduced_voxels = voxels[voxels >= min_value]
    num_points = reduced_voxels.shape[0]
    data = np.zeros(shape=(num_points, 4))
    it = np.nditer(voxels, flags=['multi_index'])
    count = 0
    while not it.finished:
        if it[0] >= 1:
            data[count, 0:3] = it.multi_index
            data[count, 3] = it[0]
            count += 1
        it.iternext()
    return data


def make_las_file(data, file_name=''):
    my_header = header.Header()
    outFile = File(file_name, mode='w', header=my_header)

    outFile.header.offset = np.amin(data, axis=0)[0:3]
    outFile.header.scale = [1, 1, 1]

    outFile.x = data[:, 0]
    outFile.y = data[:, 1]
    outFile.z = data[:, 2]
    outFile.raw_classification = data[:, 3]

    outFile.close()


def add_parser(subparser):
    parser = subparser.add_parser(
        "np2las", help="Transforms a voxel numpy grid to a point cloud", formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument("--voxel", "-v", type=str, required=True,
                        help="path to numpy voxel map")
    parser.add_argument("--min_value", "-m", type=int,
                        default=1, help="minimum cell value to create a point")
    parser.add_argument("--out", "-o", type=str, default="voxel_map.las",
                        help="path to output las file")

    parser.set_defaults(func=main)


def main(args):
    voxel_file = np.load(args.voxel)
    print("Converting voxel map to pointcloud (may take a while)")
    voxel_points = voxel_to_numpy(voxel_file)
    print("Outputting to las file")
    make_las_file(voxel_points, args.out)
