import os
import sys
import time
import argparse
import logging

import numpy as np
from createvoxel import Voxelizer

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch = logging.StreamHandler()
ch.setFormatter(formatter)
logger.addHandler(ch)


def add_parser(subparser):
    parser = subparser.add_parser(
        "voxel", help="Creates a voxel environment from elevation and building information", formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument("--elevation", "-e", type=str, required=True,
                        help="path to GeoTif terrain file")
    parser.add_argument("--terrain_value", "-tv", type=int, default=255,
                        help="value of a terrain cell")
    parser.add_argument("--terrain_buffer_top", "-tbt", type=int,
                        default=13.0, help="added height buffer to max terrain height")
    parser.add_argument("--terrain_ground_buffer", "-tgb", type=int, default=10,
                        help="number of cells to fill below terrain cell in voxel map")
    parser.add_argument("--building_database", "-bd", type=str, default="",
                        help="path to spatialite database of buildings")
    parser.add_argument("--building_value", "-bv", type=int, default=255,
                        help="value of a building cell")
    parser.add_argument("--table_name", "-tn", type=str, default="osm_buildings",
                        help="name of table containing building information")
    parser.add_argument("--ground_height_col", "-ghc", type=str, default="ground_height",
                        help="name of column containing ground height of building")
    parser.add_argument("--height_col", "-hc", type=str, default="height",
                        help="name of column containing height of building")
    parser.add_argument("--geometry_col", "-gc", type=str, default="geometry",
                        help="name of column containing geometry of building")
    parser.add_argument("--potential_field_depth", "-pfd", type=int, default=0,
                        help="Create a potential field of x cells radiating from buildings")
    # parser.add_argument("--potential_field_multiplier", "-pfd", type=int, default=3,
    #                     help="Create a potential field of x cells radiating outside of ")

    parser.add_argument("--out", "-o", type=str, default="voxel_map.npy",
                        help="path to output voxel map")

    parser.set_defaults(func=main)


def main(args):
    logger.debug("Arguments: %s\n", args)
    voxelizer = Voxelizer(
        args.elevation, buffer_top=args.terrain_buffer_top)
    logger.info("Voxelizing Terrain...")
    terrain_voxel = voxelizer.voxelize_terrain(
        ground_buffer=args.terrain_ground_buffer, terrain_value=args.terrain_value)
    logger.info("Voxelizing Terrain finished")

    building_voxels = None
    potential_field_voxels = None

    # Check if we want to voxelize buildings as well
    if args.building_database:
        kwargs = dict(file_path=args.building_database, table_name=args.table_name, ground_height_col=args.ground_height_col,
                      height_col=args.height_col, geometry_col=args.geometry_col)
        logger.info("Getting building geometry")
        buildings = voxelizer.prepare_buildings(**kwargs)

        logger.info("Voxelizing buildings...")
        building_voxels = voxelizer.voxelize_buildings(
            buildings, value=args.building_value)
        logger.info("Voxelizing buildings finished")

    if args.potential_field_depth:
        logger.debug(
            "Creating expanded building geometries for potential field")
        voxelizer.get_expanded_buildings(max_cells=args.potential_field_depth)
        logger.debug(
            "Finished creating expanding building geometries for potential field")
        logger.info("Creating potential field around buildings...")
        potential_field_voxels = voxelizer.create_potential_field()
        logger.info("Finished creating potential field")

    all_voxels = voxelizer.combine_voxels(
        terrain_voxel, building_voxels, potential_field_voxels)
    logger.info("Saving voxel file into %s", args.out)
    np.save(args.out, all_voxels)
