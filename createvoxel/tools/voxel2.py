import os
import sys
import time
import argparse
import logging
import json

import numpy as np
from createvoxel import Voxelizer

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch = logging.StreamHandler()
ch.setFormatter(formatter)
logger.addHandler(ch)


def add_parser(subparser):
    parser = subparser.add_parser(
        "voxel2", help="Creates a voxel environment from elevation and simple building information", formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument("--elevation", "-e", type=str, required=True,
                        help="path to GeoTif terrain file")
    parser.add_argument("--terrain_value", "-tv", type=int, default=255,
                        help="value of a terrain cell")
    parser.add_argument("--terrain_buffer_top", "-tbt", type=int,
                        default=14.0, help="added height buffer to max terrain height")
    parser.add_argument("--terrain_ground_buffer", "-tgb", type=int, default=10,
                        help="number of cells to fill below terrain cell in voxel map")
    parser.add_argument("--max_height_diff", type=int, default=122, help="Max height the voxel grid can be in meters")
    parser.add_argument("--potential_field_depth", "-pfd", type=int, default=0,
                        help="Create a potential field of x cells radiating from buildings")

    parser.add_argument("--out", "-o", type=str, default="voxel_map.npy",
                        help="path to output voxel map")

    parser.set_defaults(func=main)


def main(args):
    logger.debug("Arguments: %s\n", args)
    voxelizer = Voxelizer(
        args.elevation, buffer_top=args.terrain_buffer_top, max_height_diff=args.max_height_diff)
    logger.info("Voxelizing Terrain...")
    terrain_voxel = voxelizer.voxelize_terrain(
        ground_buffer=args.terrain_ground_buffer, terrain_value=args.terrain_value)
    logger.info("Voxelizing Terrain finished")

    building_voxels = None
    potential_field_voxels = None

    if args.potential_field_depth > 0:
        logger.info("Creating potential field")
        potential_field_voxels = voxelizer.create_potential_field_all(args.potential_field_depth)
        logger.info("Finished potential field")


    all_voxels = voxelizer.combine_voxels(
        terrain_voxel, building_voxels, potential_field_voxels)
    logger.info("Final shape is %s, with dtype of %s", all_voxels.shape, all_voxels.dtype)
    logger.info("Saving voxel file into %s", args.out)
    logger.info("Voxel Meta: %r", json.dumps(voxelizer.meta))
    np.save(args.out, all_voxels)
