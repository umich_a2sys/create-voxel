#!/usr/bin/env python3

import argparse

from createvoxel.tools import voxel, np2las, voxel2

def add_parsers():
    parser = argparse.ArgumentParser(prog="./cv")
    subparser = parser.add_subparsers(title="createvoxel", metavar="")

    # Add your tool's entry point below.
    voxel.add_parser(subparser)
    np2las.add_parser(subparser)
    voxel2.add_parser(subparser)


    # We return the parsed arguments, but the sub-command parsers
    # are responsible for adding a function hook to their command.
    return parser.parse_args()


if __name__ == "__main__":
    args = add_parsers()
    args.func(args)