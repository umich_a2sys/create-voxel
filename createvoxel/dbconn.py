"""Handles Spatialiate Datbase Connection

"""
import os
import sqlite3


SQL_QUERY = "SELECT {ground_height_col}, {ground_height_col} + CAST({height_col} as decimal) as total_height, ST_AsBinary(st_buffer(st_transform({geometry_col}, {srid}), ?)) as wkb FROM {table_name}"


class DBConn:
    def __init__(self, file_path, table_name='osm_buildings', ground_height_col='ground_height', height_col='height', geometry_col='geometry', srid=5555, **kwargs):
        """Initializes the database connection to find buildings
        
        Arguments:
            file_path {string} -- full path to the spatialite database
        
        Keyword Arguments:
            table_name {str} -- Name of table holding buildings (default: {'osm_buildings'})
            ground_height_col {str} -- Column name holding ground height of buildings (default: {'ground_height'})
            height_col {str} -- Column name holding building height (maximum height) (default: {'height'})
            geometry_col {str} -- Column name holding the geometry of the building (default: {'geometry'})
            srid {int} -- The coordinate system we desire to project our buildings in (coordinate system of voxel space) (default: {5555})
        
        Raises:
            Exception -- File not found for spatialite database
        """

        self.file_path = file_path
        self.table_name = table_name
        self.ground_height_col = ground_height_col
        self.height_col = height_col
        self.geometry_col = geometry_col
        self.srid = srid
        self.total_height_col = 'total_height'

        self.qry = SQL_QUERY.format(**dict(table_name=table_name, ground_height_col=ground_height_col,
                                         height_col=height_col, geometry_col=geometry_col, srid=srid))

        if not os.path.exists(file_path):
            raise Exception("File does not exist: {}".format(file_path))

        self.prep_conn()

    def get_buildings(self, buffer=0):
        """Gets buildings from the database. Applys an optional buffer radius to building geometry  
        
        Keyword Arguments:
            buffer {int} -- How much of a buffer radius around the building to apply (default: {0})
        
        Returns:
            [list] -- A list of dicts of buildings {ground_height, total_height, wkb}
        """

        result = self.conn.execute(self.qry, (buffer,))
        return result.fetchall()

    def prep_conn(self):
        """Just prepares the database connection
        """

        # Get the building data
        self.conn = sqlite3.connect(self.file_path)
        self.conn.enable_load_extension(True)
        self.conn.execute('SELECT load_extension("mod_spatialite")')
        self.conn.row_factory = lambda c, r: dict(
            zip([col[0] for col in c.description], r))
