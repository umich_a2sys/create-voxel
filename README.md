# Create Voxel

This module creates 3D voxel environments when given Geographic Information System (GIS) data. This data can include the following:

- Elevation data (required) - Form of a GeoTiff raster
- Building data (optional) - Form of a spatialite database

The end results is a three dimensional numpy array (uint8) saved as a file. This array is a _map_ that is in the same coordinate system as the elevation data. Each grid cell takes a value as specified by the user, but takes the following defaults:

| Feature  | Default Value |
| -------- | ------------- |
| terrain  | 255           |
| building | 255           |


![Voxel data of Witten, Germany](assets/voxel_point_cloud.JPG)

*Fig. 1: Voxel data of Witten, Germany. Includes elevation, building, and a potential field around buildings*

## Installation

1.  Create an isolated python environment (conda, virtualenv, etc.)
2.  `git clone https://jdcasta@bitbucket.org/umich_a2sys/create-voxel.git`
3.  `python setup.py build install`

Hopefully this worked and installed the necessary dependencies automatically. If not install these dependencies manually:

- rasterio>=1.0
- numpy
- shapely
- libspatialite

You can install these with conda as so:

```
conda install -c conda-forge rasterio numpy shapely libspatialite
```

NOTE: The `setup.py` file does not list spatialite as a dependency because there is no pypy package for spatialite.  You will have to install it manually (`apt-get install spatialite-bin  libsqlite3-mod-spatialite`) or through conda as showed above.

## Use

`createvoxel` can be used in library form by importing into python, or it can be used from the command line (CLI).

### Library

Example:

```python

from createvoxel import Voxelizer
voxelizer = Voxelizer(ELEVATION_FILE)
# Voxelize terrain
terrain_voxels = voxelizer.voxelize_terrain(terrain_value=255)
# Voxelize buildings
kwargs = dict(file_path=args.building_database, table_name=args.table_name, ground_height_col=args.ground_height_col,
              height_col=args.height_col, geometry_col=args.geometry_col)
buildings = voxelizer.prepare_buildings(**kwargs)
building_voxels = voxelizer.voxelize_buildings(buildings, value=args.building_value)
# Combine the Voxels
all_voxels = voxelizer.combine_voxels(terrain_voxels, building_voxels)
```

### CLI

The CLI can be called with the `voxel` command, which uses a digital terrain model for elevation and a spatialite database to construct buildings and apply potential fields
```
usage: cv voxel [-h] --elevation ELEVATION [--terrain_value TERRAIN_VALUE]
                  [--terrain_buffer_top TERRAIN_BUFFER_TOP]
                  [--terrain_ground_buffer TERRAIN_GROUND_BUFFER]
                  [--building_database BUILDING_DATABASE]
                  [--building_value BUILDING_VALUE] [--table_name TABLE_NAME]
                  [--ground_height_col GROUND_HEIGHT_COL]
                  [--height_col HEIGHT_COL] [--geometry_col GEOMETRY_COL]
                  [--potential_field_depth POTENTIAL_FIELD_DEPTH] [--out OUT]

optional arguments:
  -h, --help            show this help message and exit
  --elevation ELEVATION, -e ELEVATION
                        path to GeoTif terrain file (default: None)
  --terrain_value TERRAIN_VALUE, -tv TERRAIN_VALUE
                        value of a terrain cell (default: 255)
  --terrain_buffer_top TERRAIN_BUFFER_TOP, -tbt TERRAIN_BUFFER_TOP
                        added height buffer to max terrain height (default:
                        13.0)
  --terrain_ground_buffer TERRAIN_GROUND_BUFFER, -tgb TERRAIN_GROUND_BUFFER
                        number of cells to fill below terrain cell in voxel
                        map (default: 10)
  --building_database BUILDING_DATABASE, -bd BUILDING_DATABASE
                        path to spatialite database of buildings (default: )
  --building_value BUILDING_VALUE, -bv BUILDING_VALUE
                        value of a building cell (default: 255)
  --table_name TABLE_NAME, -tn TABLE_NAME
                        name of table containing building information
                        (default: osm_buildings)
  --ground_height_col GROUND_HEIGHT_COL, -ghc GROUND_HEIGHT_COL
                        name of column containing ground height of building
                        (default: ground_height)
  --height_col HEIGHT_COL, -hc HEIGHT_COL
                        name of column containing height of building (default:
                        height)
  --geometry_col GEOMETRY_COL, -gc GEOMETRY_COL
                        name of column containing geometry of building
                        (default: geometry)
  --potential_field_depth POTENTIAL_FIELD_DEPTH, -pfd POTENTIAL_FIELD_DEPTH
                        Create a potential field of x cells radiating from
                        buildings (default: 0)
  --out OUT, -o OUT     path to output voxel map (default: voxel_map.npy)
```

Or can can use the `voxel2` command, which assumes a digital surface model (DSM) for elevation (includes buildings already) and creates a potential field around **every** point.

```
usage: ./cv voxel2 [-h] --elevation ELEVATION [--terrain_value TERRAIN_VALUE]
                   [--terrain_buffer_top TERRAIN_BUFFER_TOP]
                   [--terrain_ground_buffer TERRAIN_GROUND_BUFFER]
                   [--max_height_diff MAX_HEIGHT_DIFF]
                   [--potential_field_depth POTENTIAL_FIELD_DEPTH] [--out OUT]

optional arguments:
  -h, --help            show this help message and exit
  --elevation ELEVATION, -e ELEVATION
                        path to GeoTif terrain file (default: None)
  --terrain_value TERRAIN_VALUE, -tv TERRAIN_VALUE
                        value of a terrain cell (default: 255)
  --terrain_buffer_top TERRAIN_BUFFER_TOP, -tbt TERRAIN_BUFFER_TOP
                        added height buffer to max terrain height (default:
                        14.0)
  --terrain_ground_buffer TERRAIN_GROUND_BUFFER, -tgb TERRAIN_GROUND_BUFFER
                        number of cells to fill below terrain cell in voxel
                        map (default: 10)
  --max_height_diff MAX_HEIGHT_DIFF
                        Max height the voxel grid can be in meters (default:
                        120)
  --potential_field_depth POTENTIAL_FIELD_DEPTH, -pfd POTENTIAL_FIELD_DEPTH
                        Create a potential field of x cells radiating from
                        buildings (default: 0)
  --out OUT, -o OUT     path to output voxel map (default: voxel_map.npy)
```


## Viewing Data

The recommended way of **visualizing** the data is to convert the 3D numpy array to a lidar point cloud and use [plas.io](http://plas.io/) to visualize. This conversion can be done through an included script through the following: `cv np2las -v NUMPY_FILE`. It outputs a `voxel_map.las` file. This script ignores all empty voxels and generates points only for terrain, buildings, or potential field voxels. The _classification_ of the point takes on the value of the grid cell.

The color of the points can be taken from the height of the points or through the _classification_ of the point. This can be changed through the interface as seen below (Heightmap Color or Classification):

![Plas.io screenshot of changing the colorization](assets/colormap.JPG)

*Fig. 2: Plas.io screenshot of changing the colorization*

### Voxel Viewer

Another alternative is viewing the voxels directly through this forked [repository](https://github.com/JeremyBYU/voxel-viewer).
